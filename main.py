from fastapi import FastAPI, HTTPException
import mysql.connector
from mysql.connector import Error
from pydantic import BaseModel
app = FastAPI()

def get_db_connection():
    try:
        connection = mysql.connector.connect(
            host="localhost",
            user="root",
            password="",
            database="test"
        )
        return connection
    except Error as e:
        print(f"Error connecting to MySQL database: {e}")
        raise HTTPException(status_code=500, detail="Database connection error")

@app.get("/items")
def read_items():
    connection = get_db_connection()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM items")
        items = [{"id": row[0], "name": row[1]} for row in cursor.fetchall()]
        return items
    except Error as e:
        print(f"Database query error: {e}")
        raise HTTPException(status_code=500, detail="Database query error")
    finally:
        cursor.close()
        connection.close()
class Item(BaseModel):
    name:str

@app.post("/items",response_model=Item)
def post_item(item:Item):
    db = get_db_connection()
    cursor = db.cursor()
    try:
        query = "INSERT INTO items (name) VALUES (%s)"
        cursor.execute(query, (item.name,))
        db.commit()
        return {"name":item.name}
    except Error as e:
        print(f"Database query error: {e}")
        raise HTTPException(status_code=500, detail="Database query error")

@app.put("/items/{item_id}",response_model = Item)
def update_item(item_id:int,item:Item):
    db = get_db_connection()
    cursor = db.cursor()
    try:
        query = "UPDATE items SET name= %s WHERE id=%s"
        cursor.execute(query,(item.name,item_id))
        db.commit()
        if cursor.rowcount == 0:
            raise HTTPException(status_code = 404, detail="Item not found")
        return {
            "id": item_id,
            "name": item.name
        }
    except Error as e:
        print(f"Database query error: {e}")
        raise HTTPException(status_code=500, detail="Database query error")

@app.delete("/items/{item_id}")
def delete_item(item_id:int):
    db = get_db_connection()
    cursor = db.cursor()
    try:
        query = "DELETE FROM items WHERE id= %s"
        cursor.execute(query,(item_id,))
        db.commit()
        if cursor.rowcount == 0:
            raise HTTPException (status_code=404,detail="Item not found")
        return {"message": "Item Deleted"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Database error: {str(e)}") 
    
